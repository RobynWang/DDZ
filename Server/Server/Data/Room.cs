﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ddz;

namespace Server
{
    public class Room
    {
        public const int PlayerCount = 1;

        public int roomId;
        public int landloarId;

        public int lastPutSeatId;
        public int curSeatId = 0;

        List<int> dontRobList = new List<int>();
        Dictionary<int,UserToken> users = new Dictionary<int, UserToken>();
        List<string> PaisData = new List<string>();

        Random random = new Random();

        public Room()
        {
            roomId = CacheManager.Instance.NextRooID;
        }

        //下一个回合seatId
        int NextSeatId()
        {
            curSeatId++;
            if (curSeatId >= PlayerCount)
            {
                curSeatId = 0;
            }
            return curSeatId;
        }

        int GetOneSeatId()
        {
            for (int i = 0; i < 3; i++)
            {
                if(!users.ContainsKey(i))
                {
                    return i;
                }
            }
            return 0;
        }

        //获取玩家数量
        public int UserCount
        {
            get
            {
                return users.Count;
            }
        }
        //添加玩家
        public bool AddUser(UserToken token)
        {
            if(users.Count >= PlayerCount)
            {
                return false;
            }

            int seatId = GetOneSeatId();
            users.Add(seatId, token);
            token.userInfo.roomId = this.roomId;
            token.userInfo.seatId = seatId;

            //对刚进来的玩家，通知房间信息和老玩家列表
            PBRoom pb = new PBRoom();
            pb.roomId = token.userInfo.roomId;
            pb.seatId = token.userInfo.seatId;
            for (int i = 0; i < users.Count; i++)
            {
                pb.users.Add(this.users[i].userInfo.ToPB());
            }
            NetworkManager.Instance.Send(token, Protocol.ReqEnterRoom, pb);


            //对房间内的老玩家，广播新玩家进入
            PBUser pbu = new PBUser();
            pbu = token.userInfo.ToPB();
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i] != token)
                {
                    NetworkManager.Instance.Send(users[i], Protocol.ReqOtherEnterRoom, pbu);
                }
            }
            return true;
        }

        //移除玩家
        public void RemoveUser(UserToken token)
        {
            //广播离线
            Broad(Protocol.ReqExitRoom, token.userInfo.ToPB());

            //移除玩家
            if (users.ContainsKey(token.userInfo.seatId))
            {
                users.Remove(token.userInfo.seatId);
            }
        }

        //添加准备
        public void AddReady(UserToken token)
        {
            token.userInfo.isReady = true;

            //广播有人准备游戏
            PBUser pb = token.userInfo.ToPB();
            Broad(Protocol.ReqReady, pb);

            //检测全部准备
            if(CheckAllReady())
            {
                StartGame();
            }
        }

        //移除准备
        public void RemoveReady(UserToken token)
        {
            token.userInfo.isReady = false;
        }

        //抢地主
        public void RobLandload(UserToken client)
        {
            landloarId = client.userInfo.seatId;
            curSeatId = landloarId;
            lastPutSeatId = landloarId;//赋值上一个出牌人
            client.userInfo.isLanloard = true;

            PBSeatId pb = new PBSeatId();
            pb.seatId = landloarId;
            Broad(Protocol.ReqRobLandlord, pb);


            //发地主牌
            PBPaiArray pbA = new PBPaiArray();
            pbA.paiArray.AddRange(PaisData);

            client.paiArray.AddRange(PaisData);
            NetworkManager.Instance.Send(client, Protocol.ReqGiveCard, pbA);
        }

        //不抢地主
        public void DontRobLandload(UserToken client)
        {
            int curSeatId = client.userInfo.seatId;
            //广播不抢
            PBSeatId pb = new PBSeatId();
            pb.seatId = curSeatId;
            Broad(Protocol.ReqNoRobLandlord, pb);

            //轮到下个人抢地主
            //dontRobList.Add(seatId);
            //if (dontRobList.Count == 3)
            //{
            //    //都不抢，重新开始游戏
            //}
            //else
            {
                curSeatId++;
                if(curSeatId >= PlayerCount)
                {
                    curSeatId = 0;
                }

                //下一个人继续抢地主
                PBSeatId pb1 = new PBSeatId();
                pb1.seatId = curSeatId;
                Broad(Protocol.ReqTurnToRobLandlord, pb1);
            }
        }

        //玩家出牌
        public void PlayerPutCard(UserToken client,List<string> pais)
        {
            lastPutSeatId = client.userInfo.seatId;//赋值上一个出牌人

            for (int i = 0; i < pais.Count; i++)
            {
                users[client.userInfo.seatId].paiArray.Remove(pais[i]);
            }

            //广播出牌
            PBPaiArray pb = new PBPaiArray();
            pb.paiArray.AddRange(pais);
            pb.seatId = client.userInfo.seatId;
            Broad(Protocol.ReqPutCard, pb);

           //检测游戏结束
            if( CheckEnd(lastPutSeatId))
            {
                GameOver(lastPutSeatId);
            }
            else
            {
                //下个人出牌
                PBPut put = new PBPut();
                put.seatId = NextSeatId();
                put.canPass = lastPutSeatId != put.seatId;
                Broad(Protocol.ReqTurnToPutCard, put);
            }
        }

        //玩家不要
        public void PlayerPass(UserToken client)
        {
            //广播不要
            PBSeatId pb = new PBSeatId();
            pb.seatId = client.userInfo.seatId;
            Broad(Protocol.ReqPass, pb);

            //下个人出牌
            PBPut put = new PBPut();
            put.seatId = NextSeatId();
            put.canPass = lastPutSeatId != put.seatId;
            Broad(Protocol.ReqTurnToPutCard, put);
        }

        //玩家点击继续游戏，下一局
        public void AddContinue(UserToken client)
        {
            NetworkManager.Instance.Send(client, Protocol.ReqContinue);

            AddReady(client);
        }
        //检测是否全部准备
        bool CheckAllReady()
        {
            int count = 0;
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].userInfo.isReady)
                {
                    count++;
                }
            }
            //全部准备，开始游戏
            if (count == PlayerCount)
            {
                return true;
            }
            return false;
        }

        //开始游戏
        void StartGame()
        {
            LogManager.Log(roomId+"开始游戏");

            //给房间内成员广播开始游戏
           int robSeatId = random.Next(0, PlayerCount);
            //洗牌
            List<List<string>> array = ResetPais();
            for (int i = 0; i < users.Count; i++)
            {
                PBPaiArray pb = new PBPaiArray();
                pb.seatId = robSeatId;
                pb.paiArray.AddRange(array[i]);

                users[i].paiArray = array[i];
                NetworkManager.Instance.Send(users[i], Protocol.ReqGameStart, pb);
            }
        }

        //游戏结束
        void GameOver(int winId)
        {
            LogManager.Log(roomId + "游戏结束");

            PBEnd end = new PBEnd();
            for (int i = 0; i < users.Count; i++)
            {
                PBResult result = new PBResult();
                result.seatId = i;
                result.nickname = users[i].userInfo.nickname;
                result.isLanloard = users[i].userInfo.isLanloard;
                result.score = EndScore(i, curSeatId, landloarId);
                result.paiArray.AddRange(users[i].paiArray);

                end.results.Add(result);
            }
            Broad(Protocol.ReqGameOver, end);

            //清除游戏
            ClearGame();
        }

        //检测游戏结束
        bool CheckEnd(int curSeatId)
        {
            if (users[curSeatId].paiArray.Count == 0)
            {
                return true;
            }
            return false;
        }

        //计算玩家分数
        int EndScore(int seatId,int winSeatId,int landloadId)
        {
            if(landloadId == winSeatId)
            {
                if(seatId==landloadId)
                {
                    return 200;
                }
                return -100;
            }
            else
            {
                if (seatId == landloadId)
                {
                    return -200;
                }
                return 100;
            }
        }

        //清除上一局准备等数据
        void ClearGame()
        {
            for (int i = 0; i < users.Count; i++)
            {
                users[i].userInfo.isLanloard = false;
                users[i].userInfo.isReady = false;
                users[i].paiArray.Clear();
            }
        }
        //洗牌算法
        public List<List<string>> ResetPais()
        {
            PaisData.Clear();

            //初始化牌
            for (int i = 1; i <= 4; i++)
            {
                for (int j = 1; j <= 13; j++)
                {
                    PaisData.Add(i + "_" + j);
                }
            }
            PaisData.Add("5_17");
            PaisData.Add("5_18");

            //洗牌
            List<List<string>> array = new List<List<string>>();
            for (int i = 0; i < 3; i++)
            {
                List<string> cards = new List<string>();
                for (int j = 0; j < 17; j++)
                {
                    int randIndex = random.Next(0, PaisData.Count);
                    string paiId = PaisData[randIndex];
                    PaisData.RemoveAt(randIndex);

                    cards.Add(paiId);
                }

                array.Add(cards);
            }

            return array;
        }

        //房间广播
        public void Broad<T>(Protocol protocol,T t) where T : ProtoBuf.IExtensible
        {
            foreach (var item in users)
            {
                NetworkManager.Instance.Send(item.Value, protocol, t);
            }
        }
    }
}
