﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class User
    {
        public string userId;
        public string nickname;
        public string icon;
        public int roomId; //房间ID
        public int seatId;//座位号
        public int score;//分数
        public bool isLanloard;//地主
        public bool isReady;//准备
        
        public ddz.PBUser ToPB()
        {
            ddz.PBUser pb = new ddz.PBUser();

            pb.userId = this.userId;
            pb.nickname = this.nickname;
            pb.icon = this.icon;
            pb.roomId = this.roomId;
            pb.seatId = this.seatId;
            pb.score = this.score;
            pb.isLanloard = this.isLanloard;
            pb.isReady = this.isReady;

            return pb;
        }
    }
}
