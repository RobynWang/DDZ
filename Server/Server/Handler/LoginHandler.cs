﻿using System;
using System.Collections.Generic;
using System.Text;
using ddz;

namespace Server
{
    public class LoginHandler : IEvent
    {
        public void RegisterEvent(HandlerCenter center)
        {
            center.AddEvent(Protocol.ReqLogin, OnLogin);
        }

        //登录
        private void OnLogin(UserToken client, byte[] data)
        {
            LogManager.Log("登录成功");
            PBAccount acc = Util.Deserilize<PBAccount>(data);
            client.userInfo.userId = acc.userId;
            client.userInfo.nickname = acc.nickname;
            client.userInfo.icon = acc.icon;

            CacheManager.Instance.OnLine(client);
            NetworkManager.Instance.Send(client, Protocol.ReqLogin, acc);
        }


        //下线
        public void OnLogOff(UserToken client, string error)
        {
            LogManager.Log(client.userInfo.nickname + "下线");
            LogManager.Log(error);

            CacheManager.Instance.Offline(client);
            CacheManager.Instance.RemoveRoomUser(client);
        }
    }
}
