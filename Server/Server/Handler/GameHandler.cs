﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ddz;

namespace Server
{
    class GameHandler : IEvent
    {
        public void RegisterEvent(HandlerCenter center)
        {
            center.AddEvent(Protocol.ReqReady,OnPlayerReady);
            center.AddEvent(Protocol.ReqRobLandlord, OnPlayerRobLandlord);
            center.AddEvent(Protocol.ReqNoRobLandlord, OnPlayerNoRobLandlord);
            center.AddEvent(Protocol.ReqPutCard, OnPlayerPutCard);
            center.AddEvent(Protocol.ReqPass, OnPlayerPass);
            center.AddEvent(Protocol.ReqContinue, OnGameContinue);
        }


        private void OnPlayerReady(UserToken client, byte[] data)
        {
            int roomId = client.userInfo.roomId;
            LogManager.Log(roomId + "玩家准备");

            Room room;
            if (CacheManager.Instance.gameRooms.TryGetValue(roomId, out room))
            {
                room.AddReady(client);
            }
        }
        private void OnPlayerRobLandlord(UserToken client, byte[] data)
        {
            int roomId = client.userInfo.roomId;
            LogManager.Log(roomId + "玩家抢地主");

            Room room;
            if (CacheManager.Instance.gameRooms.TryGetValue(roomId, out room))
            {
                room.RobLandload(client);
            }
        }

        private void OnPlayerNoRobLandlord(UserToken client, byte[] data)
        {
            int roomId = client.userInfo.roomId;
            LogManager.Log(roomId + "玩家不抢地主");

            Room room;
            if (CacheManager.Instance.gameRooms.TryGetValue(roomId, out room))
            {
                room.DontRobLandload(client);
            }
        }

        private void OnPlayerPutCard(UserToken client, byte[] data)
        {
            PBPaiArray pb = Util.Deserilize<PBPaiArray>(data);

            int roomId = client.userInfo.roomId;
            LogManager.Log(roomId + "玩家出牌");

            Room room;
            if (CacheManager.Instance.gameRooms.TryGetValue(roomId, out room))
            {
                room.PlayerPutCard(client, pb.paiArray);
            }
        }

        private void OnPlayerPass(UserToken client, byte[] data)
        {
            int roomId = client.userInfo.roomId;
            LogManager.Log(roomId + "玩家不要");

            Room room;
            if (CacheManager.Instance.gameRooms.TryGetValue(roomId, out room))
            {
                room.PlayerPass(client);
            }
        }

        //重新开始游戏
        private void OnGameContinue(UserToken client, byte[] data)
        {
            int roomId = client.userInfo.roomId;
            LogManager.Log(roomId + "重新准备");

            Room room;
            if (CacheManager.Instance.gameRooms.TryGetValue(roomId, out room))
            {
                room.AddContinue(client);
            }
        }
    }
}
