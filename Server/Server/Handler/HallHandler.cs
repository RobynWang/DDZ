﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class HallHandler:IEvent
    {
        public void RegisterEvent(HandlerCenter center)
        {
            center.AddEvent(Protocol.ReqEnterRoom, OnEnterRoom);
            center.AddEvent(Protocol.ReqExitRoom, OnExitRoom);
        }

        private void OnEnterRoom(UserToken client, byte[] data)
        {
            LogManager.Log(client.userInfo.nickname+"玩家进入房间");

            //获取不满的房间
            Room room = CacheManager.Instance.GetOneReadyRoom();
            room.AddUser(client);
        }

        private void OnExitRoom(UserToken client, byte[] data)
        {
            int roomId = client.userInfo.roomId;
            LogManager.Log(client.userInfo.nickname + "退出房间");

            Room room;
            if (CacheManager.Instance.gameRooms.TryGetValue(roomId, out room))
            {
                room.RemoveUser(client);
            }
        }
    }
}
