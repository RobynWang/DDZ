﻿using System;
using System.IO;

namespace Server
{
    public static class Util
    {
        /// <summary>
        /// protobuf的序列化（实体类->byte流）
        /// </summary>
        /// <typeparam name="T">protobuf的实体类型</typeparam>
        /// <param name="t">实体对象</param>
        /// <returns></returns>
        public static byte[] Serilize<T>(T t) where T : ProtoBuf.IExtensible
        {
            MemoryStream stream = new MemoryStream();
            ProtoBuf.Serializer.Serialize(stream, t);
            return stream.ToArray();
        }

        /// <summary>
        /// protobuf的反序列化（bytes流->实体类）
        /// </summary>
        /// <typeparam name="T">protobuf的实体类型</typeparam>
        /// <param name="data">数据流</param>
        /// <returns></returns>
        public static T Deserilize<T>(byte[] data) where T : ProtoBuf.IExtensible
        {
            MemoryStream stream = new MemoryStream(data);
            T t = ProtoBuf.Serializer.Deserialize<T>(stream);
            return t;
        }
    }
}
