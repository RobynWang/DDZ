﻿using System;
using System.Collections.Generic;

namespace Server
{
    public class HandlerCenter :IHandlerCenter
    {
        LoginHandler account;
        HallHandler hall;
        GameHandler game;

        /// <summary>
        /// 存储所有的protocol协议和绑定函数
        /// </summary>
        Dictionary<Protocol, NetEvent> handlers = new Dictionary<Protocol, NetEvent>();

        /// <summary>
        /// 初始化消息分发
        /// </summary>
        public void Initialize()
        {
            account = new LoginHandler();
            account.RegisterEvent(this);

            hall = new HallHandler();
            hall.RegisterEvent(this);

            game = new GameHandler();
            game.RegisterEvent(this);
        }

        /// <summary>
        /// 添加回调
        /// </summary>
        /// <param name="cmd">协议</param>
        /// <param name="func">绑定函数</param>
        public void AddEvent(Protocol cmd,NetEvent func)
        {
            handlers.Add(cmd,func);
        }

        /// <summary>
        /// 客户端断开连接
        /// </summary>
        /// <param name="token">客户端token</param>
        /// <param name="error">异常</param>
        public void ClientClose(UserToken token, string error)
        {
            account.OnLogOff(token, error);
        }

        /// <summary>
        /// 客户端连接
        /// </summary>
        /// <param name="token">客户端token</param>
        public void ClientConnect(UserToken token)
        {
            
        }

        /// <summary>
        /// 接受消息，根据cmd分发消息
        /// </summary>
        /// <param name="client"></param>
        /// <param name="buffer"></param>
        public void MessageReceive(UserToken client,ByteBuffer buffer)
        {
            Protocol cmd = (Protocol)buffer.ReadShort();//协议占2字节
            byte[] data = buffer.ReadBytes();
            buffer.Close();

            NetEvent func;
            if (handlers.TryGetValue(cmd, out func))
                func(client, data);
            else
                LogManager.Log("协议未绑定:" + cmd);
        }
    }
}
