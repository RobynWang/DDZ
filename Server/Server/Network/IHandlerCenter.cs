﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Server
{
    public interface IHandlerCenter
    {
        // 初始化消息操纵器
        void Initialize();

        // 客户端连接
        void ClientConnect(UserToken token);

        // 客户端断开
        void ClientClose(UserToken token, string error);

        //绑定事件
        void AddEvent(Protocol cmd, NetEvent func);

        // 收到消息
        void MessageReceive(UserToken token, ByteBuffer buffer);
    }


    public interface IEvent
    {
        void RegisterEvent(HandlerCenter center);
    }

    public delegate void NetEvent(UserToken client, byte[] data);
}
