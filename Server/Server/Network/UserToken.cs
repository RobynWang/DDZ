﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    public class UserToken
    {
        private const int BufferSize = 1024 * 4;//声明buffersize常量
        private byte[] buffer = new byte[BufferSize];

        TcpClient client;
        NetworkStream stream;

        MemoryStream memStream;
        BinaryReader reader;
        IHandlerCenter center;

        public User userInfo = new User();
        public List<string> paiArray = new List<string>();

        public int timeout = 0;//网络超时计时

        public UserToken(TcpClient client,IHandlerCenter center)
        {
            this.client = client;
            this.center = center;

            //获取网络流
            stream = client.GetStream();

            //初始化内存流
            memStream = new MemoryStream();
            reader = new BinaryReader(memStream);

            //开启异步读取流
            stream.BeginRead(buffer, 0, BufferSize, OnReadCallBack, null);

            OnConnect();
        }

        void OnReadCallBack(IAsyncResult ar)
        {
            try
            {
                //结束异步读取
                int readCount = stream.EndRead(ar);
                if (readCount < 1)
                {
                    OnDisConnect("流长度异常，客户端断开!");
                    return;
                }

                OnReceive(buffer, readCount);

                lock (stream)//再次开启异步读取
                {
                    Array.Clear(buffer, 0, BufferSize);
                    stream.BeginRead(buffer, 0, BufferSize, OnReadCallBack, null);
                }
            }
            catch (IOException io)
            {
                OnDisConnect("IO异常，客户端断开!");
            }
            catch (Exception e)
            {
                LogManager.Log(e);
            }
        }

        void OnConnect()
        {
            LogManager.Log(client.Client.RemoteEndPoint+" connect");
        }
        public void OnDisConnect(string error)
        {
            LogManager.Log(client.Client.RemoteEndPoint + " disconnect");
            center.ClientClose(this, error);

            Close();
        }

        void Close()
        {
            stream.Close();
            client.Close();
        }

        //粘包处理
        void OnReceive(byte[] bytes, int length)
        {
            //数据的追加
            memStream.Seek(0, SeekOrigin.End);
            memStream.Write(bytes, 0, length);
            memStream.Seek(0, SeekOrigin.Begin);

            int lengthCount = 2;//short类型占2个字节
            while (RemainingBytes() > lengthCount)
            {
                int messageLength = reader.ReadUInt16();
                if (RemainingBytes() >= messageLength)
                {
                    byte[] result = reader.ReadBytes(messageLength);
                    OnMessage(result);
                }
                else
                {
                    memStream.Position = memStream.Position - lengthCount;
                    break;
                }
            }

            //把剩余的字节保存起来，已经读取的字节全部丢弃
            byte[] leftBytes = reader.ReadBytes((int)RemainingBytes());
            memStream.SetLength(0);
            memStream.Write(leftBytes,0,leftBytes.Length);
        }

        long RemainingBytes()
        {
            return memStream.Length - memStream.Position;
        }

        //接受到一条消息的流
        void OnMessage(byte[] result)
        {
            ByteBuffer buff = new ByteBuffer(result);
            center.MessageReceive(this, buff);
        }


        void OnWrite(byte[] bytes)
        {
            ByteBuffer buff = new ByteBuffer();
            buff.WriteShort((ushort)bytes.Length);
            buff.WriteBytes(bytes);

            byte[] result = buff.ToBytes();
            stream.Write(result, 0, result.Length);
            buff.Close();
        }

        public void Send(ushort cmd, byte[] content)
        {
            ByteBuffer buff = new ByteBuffer();
            buff.WriteShort((ushort)cmd);//写入时ushort占用2字节
            buff.WriteBytes(content);

            OnWrite(buff.ToBytes());
            buff.Close();
        }
    }
}
