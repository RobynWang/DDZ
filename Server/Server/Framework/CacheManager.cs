﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// partial:同一个类，分不同cs文件写
/// </summary>
namespace Server
{
    public abstract class XModule { }

    //已登录用户的数据
    public partial class CacheManager : Singleton<CacheManager>
    {
        public static int ROOMID_CACHE = 10000;

        public List<UserToken> tokens = new List<UserToken>();
        public Dictionary<int, Room> gameRooms = new Dictionary<int, Room>();

        public int NextRooID
        {
            get
            {
                return ROOMID_CACHE++;
            }
        }

        public Room GetOneReadyRoom()
        {
            //获取可用房间
            foreach (var item in gameRooms)
            {
                if (item.Value.UserCount < 3)
                {
                    return item.Value;
                }
            }

            //创建新房间
            Room room = new Room();
            Instance.gameRooms.Add(room.roomId, room);

            return room;
        }

        public void OnLine(UserToken token)
        {
            tokens.Add(token);
        }
        public void Offline(UserToken token)
        {
            if (tokens.Contains(token))
                tokens.Remove(token);
        }

        public void RemoveRoomUser(UserToken token)
        {
            int roomId = token.userInfo.roomId;
            if (roomId != 0)
            {
                if (Instance.gameRooms.ContainsKey(roomId))
                {
                    Instance.gameRooms[roomId].RemoveUser(token);
                }
            }
        }
    }
}
