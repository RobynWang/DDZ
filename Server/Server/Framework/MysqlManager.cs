﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

/// <summary>
/// Mysql管理器
/// 1.引入mysql的引用库
/// </summary>
namespace Server
{
    class MysqlManager : Singleton<MysqlManager>
    {
        const string ip = "127.0.0.1";
        const string username = "root";
        const string password = "123456";
        const string dbName = "arpg";

        MySqlConnection connect;//数据库连接器
        MySqlCommand command;//数据库命令

        public void Init()
        {
            try
            {
                connect = new MySqlConnection();
                connect.ConnectionString = string.Format("server={0}; user id={1}; password={2}; database={3};", ip, username, password, dbName);

                connect.Open();
                command = connect.CreateCommand();//命令初始化，用于增删改查
                LogManager.Log("mysql:" + dbName + " connected");
            }
            catch (Exception e)
            {
                LogManager.Log(e);
            }
        }

        public void Close()
        {
            command.Dispose();
            connect.Dispose();
            LogManager.Log("mysql:" + dbName + " disconnect");
        }

        //创建表
        public void CreateTable<T>()
        {
            Type type = typeof(T);
            string sql = "create table " + type.Name + "(";
            var fields = type.GetFields();

            for (int i = 0; i < fields.Length; i++)
            {
                sql += fields[i].Name + " " + CS2DB(fields[i].FieldType.Name);
                if (i == 0)
                {
                    sql += " not null,PRIMARY KEY(" + fields[i].Name + ")";
                }
                if (i < fields.Length - 1)
                {
                    sql += ",";
                }
            }
            sql += ");";
            ExecuteNonQuery(sql);
        }

        string CS2DB(string type)
        {
            switch (type)
            {
                case "String":
                    return "varchar(20)";
                case "Int32":
                    return "int(10)";
                case "Single":
                    return "float(10)";
                default:
                    break;
            }
            return "";
        }
        //增加数据1.0
        public void InsertInto(string tableName, params object[] args)
        {
            string sql = "insert into " + tableName + " values(";
            for (int i = 0; i < args.Length; i++)
            {
                sql += "'" + args[i] + "'";

                if (i < args.Length - 1)
                {
                    sql += ",";
                }
            }
            sql += ");";
            ExecuteNonQuery(sql);
        }

        //增加数据2.0
        public void InserInto<T>(T t,string tableName=null) where T:XModule
        {
            Type type = typeof(T);
            if(tableName == null)
            {
                tableName = type.Name;
            }
            var fields = type.GetFields();//无序的

            string sql = "insert into " + tableName+"(";

            for (int i = 0; i < fields.Length; i++)
            {
                sql += " " + fields[i].Name+ "";
                if (i < fields.Length - 1)
                {
                    sql += ",";
                }
            }
            sql += ")values(";

            for (int i = 0; i < fields.Length; i++)
            {
                sql += " '" + fields[i].GetValue(t) + "'";
                if (i < fields.Length - 1)
                {
                    sql += ",";
                }
            }
            sql += ");";
            ExecuteNonQuery(sql);
        }

        //删除数据1.0
        public void DeleteFrom(string tableName, object value, string key = "Id")
        {
            string sql = "delete from " + tableName + " where " + key + "='" + value + "';";
            ExecuteNonQuery(sql);
        }

        //删除数据2.0
        //public void DeleteFrom<T>(object value, string key = "Id")
        //{
        //    string tableName = typeof(T).Name;
        //    string sql = "delete from " + tableName + " where " + key + "='" + value + "';";
        //    ExecuteNonQuery(sql);
        //}

        //修改数据1.0
        public void UpdateSet(string tableName, params object[] args)
        {
            string sql = "update " + tableName + " set ";
            if (args.Length == 4)
            {
                sql += "'" + args[0] + "' = '" + args[1] + "' ";//修改结果
                sql += " where '" + args[2] + "' = '" + args[3] + "'";//条件

                ExecuteNonQuery(sql);
            }
            else
            {
                LogManager.Log("args 参数长度不够");
            }
        }
        //修改数据2.0
        public void UpdateSet<T>(T t, string tableName = null)
        {
            Type type = typeof(T);

            if (tableName == null)//添加2.5版本 传表名
            {
                tableName = type.Name;
            }
            string sql = "update " + tableName + " set ";

            var fields = type.GetFields();
            for (int i = 1; i < fields.Length; i++)
            {
                sql += fields[i].Name + "='" + fields[i].GetValue(t) + "'";
                if (i < fields.Length - 1)
                {
                    sql += ",";
                }
            }

            var IdField = fields[0];
            sql += " where " + IdField.Name + "='" + IdField.GetValue(t) + "';";

            ExecuteNonQuery(sql);
        }

        //查询1.0
        public Dictionary<string, object> SelectFrom(string tableName, object value, string key = "Id")
        {
            string sql = "select * from " + tableName + " where " + key + "='" + value + "';";
            var reader = ExecuteQuery(sql);

            Dictionary<string, object> dic = new Dictionary<string, object>();

            //读取第一条数据
            reader.Read();
            int count = reader.FieldCount;//获取列的数量
            for (int i = 0; i < count; i++)
            {
                dic.Add(reader.GetName(i), reader.GetValue(i));
            }

            reader.Close();
            return dic;
        }

        //1.5查询
        public T SelectFrom<T>(string sql) where T : new()
        {
            Type type = typeof(T);

            var reader = ExecuteQuery(sql);

            T t = default(T);
            if (reader.Read())//指向第一条数据
            {
                t = new T();
                for (int i = 0; i < reader.FieldCount; i++)//获取数据库表总列数
                {
                    string tKey = reader.GetName(i);//获取第i列的 列名
                    object tValue = reader.GetValue(i);//获取第i列的 列值

                    //Reflection.FieldInfo
                    var field = type.GetField(tKey);//根据列名获取T的同名字段
                    field.SetValue(t, tValue);//根据字段FieldInfo给t对象赋值该字段
                }
            }

            reader.Close();
            return t;//T类型的默认值，int=0，string="",class=null;
        }

        //查询2.0
        public T SelectFrom<T>(object value, string key = "Id",string tableName=null) where T : new()
        {
            Type type = typeof(T);
            if (tableName == null)//2.5版本
            {
                tableName = type.Name;
            }

            string sql = "select * from " + tableName + " where " + key + "='" + value + "';";
            var reader = ExecuteQuery(sql);

            T t = default(T);
            if (reader.Read())//指向第一条数据
            {
                t= new T();
                for (int i = 0; i < reader.FieldCount; i++)//获取数据库表总列数
                {
                    string tKey = reader.GetName(i);//获取第i列的 列名
                    object tValue = reader.GetValue(i);//获取第i列的 列值

                    //Reflection.FieldInfo
                    var field = type.GetField(tKey);//根据列名获取T的同名字段
                    field.SetValue(t, tValue);//根据字段FieldInfo给t对象赋值该字段
                }
            }

            reader.Close();
            return t;//T类型的默认值，int=0，string="",class=null;
        }


        //查询所有1.0
        public List<Dictionary<string, object>> SelectAllFrom(string tableName)
        {
            string sql = "select * from " + tableName + ";";
            var reader = ExecuteQuery(sql);

            List<Dictionary<string, object>> userList = new List<Dictionary<string, object>>();

            while (reader.Read())//往下读一条，然后reader指向当前这条数据
            {
                Dictionary<string, object> user = new Dictionary<string, object>();
                int count = reader.FieldCount;//获取列的数量
                for (int i = 0; i < count; i++)
                {
                    user.Add(reader.GetName(i), reader.GetValue(i));//给字典赋值每一个列的字段
                    //LogManager.Log(reader.GetName(i)+":"+ reader.GetValue(i));//列名+列值
                }
                userList.Add(user);//把一个用户数据加入字典
            }

            reader.Close();
            return userList;
        }

        //查询所有2.0
        public List<T> SelectAllFrom<T>(string tableName=null) where T : new()
        {
            Type type = typeof(T);
            if(tableName==null)//2.5版本
            {
                tableName = type.Name;
            }
            string sql = "select * from " + tableName + ";";

            List<T> list = new List<T>();
            var reader = ExecuteQuery(sql);
            while (reader.Read())
            {
                T t = new T();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string key = reader.GetName(i);//username
                    object value = reader.GetValue(i);

                    var field = type.GetField(key);//username的字段
                    field.SetValue(t, value);
                }
                list.Add(t);
            }
            reader.Close();
            return list;
        }

        /// <summary>
        /// 根据Id查询多个数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName"></param>
        /// <param name="value"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<T> SelectAllFrom<T>(string tableName,object value,string key="Id") where T : new()
        {
            Type type = typeof(T);
            if (tableName == null)//根据表名参数，决定是否使用T的名字做表名
            {
                tableName = type.Name;
            }
            string sql = "select * from " + tableName + " where " + key + "='" + value+"';";

            List<T> list = new List<T>();
            var reader = ExecuteQuery(sql);
            while (reader.Read())
            {
                T t = new T();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string tKey = reader.GetName(i);//username
                    object tValue = reader.GetValue(i);

                    var field = type.GetField(tKey);//username的字段
                    field.SetValue(t, tValue);
                }
                list.Add(t);
            }
            reader.Close();
            return list;
        }

        void ExecuteNonQuery(string sql)
        {
            LogManager.Log("mysql: Excute->" + sql);

            command.CommandText = sql;
            command.ExecuteNonQuery();
        }

        MySqlDataReader ExecuteQuery(string sql)
        {
            LogManager.Log("mysql: Excute->" + sql);

            command.CommandText = sql;
            var reader = command.ExecuteReader();
            return reader;
        }
    }
}
