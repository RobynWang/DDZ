﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    public class NetworkManager : Singleton<NetworkManager>
    {
        private static object obj = new object();
        //private string ip = "127.0.0.1";//"localhost"
        private string ip = "192.168.108.34";
        private const int port = 8806;

        private TcpListener listener;
        private IPAddress address;
        private IHandlerCenter handlerCenter;

        public NetworkManager()
        {
            handlerCenter = new HandlerCenter();//消息分发器
            handlerCenter.Initialize();

            address = IPAddress.Parse(ip);
        }

        //启动服务器
        public void Init()
        {
            Console.WriteLine("please input ip：");
            ip = Console.ReadLine().ToString();
            IPAddress address = IPAddress.Parse(ip);

            listener = new TcpListener(address, port);
            listener.Start();//开始监听
            LogManager.Log(string.Format("Start Listening  ip:{0}  port:{1}", ip, port));

            //开启异步读取客户端连接
            listener.BeginAcceptTcpClient(OnAcceptTcp, null);
        }

        private void OnAcceptTcp(IAsyncResult ar)
        {
            //异步接受客户端连接
            TcpClient client = listener.EndAcceptTcpClient(ar);
            //把客户端交给 Remote脚本处理读取数据
            UserToken rm = new UserToken(client, handlerCenter);

            lock (obj)//再次开启异步读取
            {
                listener.BeginAcceptTcpClient(OnAcceptTcp, null);
            }
        }

        //T类型添加protobuf约束
        public void Send<T>(UserToken client, Protocol cmd, T t) where T : ProtoBuf.IExtensible
        {
            client.Send((ushort)cmd, Util.Serilize<T>(t));
        }

        public void Send(UserToken client, Protocol cmd)
        {
            client.Send((ushort)cmd, new byte[0]);
        }
    }
}
