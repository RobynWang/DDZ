﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Server
{
    class GameServer
    {
        public GameServer()
        {
            //日志管理器
            LogManager.Instance.Init();
            LogManager.Log("logger initalized");

            //数据库管理器
            //MysqlManager.Instance.Init();
            //LogManager.Log("mysql initalized");

            //数据缓存管理器
            DataManager.Instance.Init();
            LogManager.Log("data initalized");

            //socket服务器
            NetworkManager.Instance.Init();
            LogManager.Log("server initalized");

            //HeartManager.Instance.Init();
            //LogManager.Log("heart initalized");

            //开启loop循环，防止程序退出
            Thread loop = new Thread(Loop);
            loop.Start();
        }

        private static void Loop()
        {
            while (true)
            {
                Thread.Sleep(1000);
            }
        }
    }
}
