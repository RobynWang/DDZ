HallHandler={};
local this=HallHandler;

function HallHandler.Init(net)
    net.Handlers[Protocal.ReqEnterRoom]=this.OnEnterRoom;
    net.Handlers[Protocal.ReqExitRoom]=this.OnExitRoom;
    net.Handlers[Protocal.ReqOtherEnterRoom] = this.OnOtherEnterRoom;
end

function HallHandler.OnEnterRoom(data)

    local pbRoom = protobuf.decode("ddz.PBRoom",data);
    Game.roomId = pbRoom.roomId;
    Game.seatId = pbRoom.seatId;
    Game.users = pbRoom.users;

    log("加入房间");
    HallPanel.Close();
    GamePanel.New();
end

function HallHandler.OnExitRoom(data)
    local pb=protobuf.decode("ddz.PBUser",data);
    if Game.seatId == pb.seatId then
        GamePanel.Close();
        HallPanel.New();
    else
        Game.users[pb.seatId] = nil;
        GamePanel.ClearPlayer(pb);--删除新玩家
    end
end

function HallHandler.OnOtherEnterRoom(data)
    local pb=protobuf.decode("ddz.PBUser",data);
    Game.users[pb.seatId] = pb;
    GamePanel.InitPlayer(pb);--初始化新玩家
end