
Network = {};
local this = Network;

local islogging = false;
Network.Handlers={};

function Network.Start() 
    logWarn("Network.Start!!");
    Event.AddListener(Protocal.Connect, this.OnConnect); 
    Event.AddListener(Protocal.Message, this.OnMessage); 
    Event.AddListener(Protocal.Exception, this.OnException); 
    Event.AddListener(Protocal.Disconnect, this.OnDisconnect);

    --注册消息分发
    LoginHandler.Init(this);
    HallHandler.Init(this);
    GameHandler.Init(this);

    -- 注册pbc
    local path = Util.DataPath.."lua/3rd/pbc/data.pb";
    if(AppConst.DebugMode == 1)then
        path = AppConst.FrameworkRoot.."/lua/3rd/pbc/data.pb";
    end

    local file = io.open(path,"rb")
    local buffer = file:read "*a";
    file:close();
    protobuf.register(buffer);
end

--Socket消息--
function Network.OnSocket(key, data)
    Event.Brocast(tostring(key), data);
end

--当连接建立时--
function Network.OnConnect() 
    logWarn("Game Server connected!!");
end

--异常断线--
function Network.OnException() 
    islogging = false; 
    NetManager:SendConnect();
   	logError("OnException------->>>>");
end

--连接中断，或者被踢掉--
function Network.OnDisconnect() 
    islogging = false; 
    logError("OnDisconnect------->>>>");
end

--网络消息接收--
function Network.OnMessage(buffer) 
    local Id = buffer:ReadShort();--读取协议
    local data = buffer:ReadBuffer();--读取数据

    --消息分发
    this.Handlers[Id](data);
end

--网络消息发送--
function Network.Send(protoId, data)
    local buffer = ByteBuffer.New();
    buffer:WriteShort(protoId);
    if data ~= nil then
        buffer:WriteBuffer(data);
    end
    --交给cs网络层
    networkMgr:SendMessage(buffer);
end

--卸载网络监听--
function Network.Unload()
    Event.RemoveListener(Protocal.Connect);
    Event.RemoveListener(Protocal.Message);
    Event.RemoveListener(Protocal.Exception);
    Event.RemoveListener(Protocal.Disconnect);
    logWarn('Unload Network...');
end