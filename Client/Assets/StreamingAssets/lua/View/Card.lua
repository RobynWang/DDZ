
Card=class("Card");

function Card:ctor(go)
    self.gameObject=go;
    self.transform=go.transform;

    self.num=self.transform:Find("num"):GetComponent('Image');
    self.color=self.transform:Find("color"):GetComponent('Image');

    self.ispress = false;
    self.original = self.transform.localPosition;
end

function Card:SetPai(paiId)
    local numPath="card_"..paiId;
    local sp =	resMgr:LoadAsset("pokers",numPath ,typeof(Sprite));
    self.num.sprite=sp;

    local colorPath="color_"..string.sub(paiId, 1, 1);
    log(colorPath);
    local sp =	resMgr:LoadAsset("pokers",colorPath ,typeof(Sprite));
    self.color.sprite=sp;
end

function Card:Press()
    if self.ispress then
        self.ispress = false;
        self.transform.localPosition = self.original;
    else
        self.ispress =true;
        self.transform.localPosition = self.original + Vector3(0,30,0);
    end
end

function Card:IsShoot()
    return self.ispress;
end