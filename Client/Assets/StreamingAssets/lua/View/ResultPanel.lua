
ResultPanel={};
local this = ResultPanel;

local gameObject;
local transform;
local behaviour;

function ResultPanel.New(results)
	panelMgr:CreatePanel("Result",function()
        this.ShowScore(results);
    end);
end
function ResultPanel.Close()
	panelMgr:ClosePanel("Result");
end

function ResultPanel.Awake(obj)
    gameObject = obj;
	transform = obj.transform;
    behaviour = gameObject:GetComponent('LuaBehaviour');
    
    this.results={};
    for i=1,3 do
        local path="Player"..i.."/Text";
        this.results[i]=transform:Find(path):GetComponent('Text');
    end

    this.continueBtn = transform:Find("ContinueBtn").gameObject;
    behaviour:AddClick(this.continueBtn,function()
        Network.Send(Protocal.ReqContinue);
    end);
end

function ResultPanel.ShowScore(results)
    for i,v in ipairs(results) do
        if v.isLanloard then
            this.results[i].gameObject.transform.parent:Find("landlord").gameObject:SetActive(true);
        else
            this.results[i].gameObject.transform.parent:Find("farm").gameObject:SetActive(true);
        end

        this.results[i].text="昵称："..v.username..
        "     分数："..v.score;
    end
end