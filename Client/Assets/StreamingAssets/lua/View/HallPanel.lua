
HallPanel = {};
local this = HallPanel;

local gameObject;
local transform;
local behaviour;

function HallPanel.New()
	panelMgr:CreatePanel("Hall", nil);
end

function HallPanel.Close()
	panelMgr:ClosePanel("Hall");
end

function HallPanel.Awake(obj)
	gameObject = obj;
	transform = obj.transform;
	behaviour = gameObject:GetComponent('LuaBehaviour');
	
	this.enterBtn = transform:Find("enterBtn").gameObject;
	behaviour:AddClick(this.enterBtn, this.OnEnterClick);
end

function HallPanel.OnEnterClick()
    Network.Send(Protocal.ReqEnterRoom)
end
