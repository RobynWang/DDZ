
PlayerHandView=class("PlayerHandView",BaseView);

function PlayerHandView:ctor(go)
    self.gameObject=go;
    self.transform= go:GetComponent('RectTransform');

      --手牌的缓存
    self.paiArray={};
    self.cardList={};
  
    self.cardprefab = resMgr:LoadAsset("game",'card',typeof(GameObject));

    self.pointer = self.gameObject:AddComponent(typeof(LuaPoint));
    self.pointer:AddDownHandler(self.PointerDown,self);
end

function PlayerHandView:InsertPaiArray(array)
    for i, paiId in ipairs(array) do
        table.insert(self.paiArray, paiId)
    end

    --添加新牌
    for i,paiId in ipairs(array) do
		self:CreateCard(paiId);
    end
    --刷新牌
    self:RefreshPaiView();
end

function PlayerHandView:PointerDown()
    local flag,localPoint = RectTransformUtility.
    ScreenPointToLocalPointInRectangle(self.transform, Input.mousePosition,panelMgr.UICamera,nil);
    if(flag)then
        local x,y = Vec2Str(localPoint);
        local total = #self.paiArray;
        local i= (x+60)/60 + total/2 + 0.5;
        if(i>total and i<total+2)then
            i = total;
        else
            i = math.modf(i);
        end

        if(i<1 or i>total)then
            return;
        end
        self.cardList[self.paiArray[i]]:Press();
    end
end

function PlayerHandView:GetShootPais()
    local shoots={};
    for k,paiId in pairs(self.paiArray) do
        local card = self.cardList[paiId];
        if(card:IsShoot())then
            table.insert(shoots,paiId);
        end
    end
    return shoots;
end

function PlayerHandView:ShowDownPais()
    for paiId,card in pairs(self.cardList) do
        if card:IsShoot() then
            card:Press();
        end
    end
end