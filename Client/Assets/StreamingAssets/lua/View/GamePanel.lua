
GamePanel = {};
local this = GamePanel;

local gameObject;
local transform;
local behaviour;

function GamePanel.New()
	panelMgr:CreatePanel("Game", nil);
end

function GamePanel.Close()
	panelMgr:ClosePanel("Game");
end

function GamePanel.Awake(obj)
	gameObject = obj;
	transform = obj.transform;
	behaviour = gameObject:GetComponent('LuaBehaviour');
	
	this.readyBtn = transform:Find("readyBtn").gameObject;
	this.exitBtn = transform:Find("exitBtn").gameObject;
	this.robBtn = transform:Find("robBtn").gameObject;
	this.norobBtn = transform:Find("norobBtn").gameObject;	
	this.passBtn = transform:Find("passBtn").gameObject;
	this.putBtn = transform:Find("putBtn").gameObject;
	this.tipBtn = transform:Find("tipBtn").gameObject;

	behaviour:AddClick(this.readyBtn,function()
		Network.Send(Protocal.ReqReady)
	end);
	behaviour:AddClick(this.exitBtn,function()
		Network.Send(Protocal.ReqExitRoom)
	end);
	behaviour:AddClick(this.robBtn,function()
		Network.Send(Protocal.ReqRobLandlord)
	end);
	behaviour:AddClick(this.norobBtn,function()
		Network.Send(Protocal.ReqNoRobLandlord)
	end);
	behaviour:AddClick(this.passBtn,function()
		Network.Send(Protocal.ReqPass)
	end);
	behaviour:AddClick(this.putBtn,function()
		local putpais = this.Players[1]:GetPressPai()
		if next(putpais) == nil then
			return;
		end
		if this.CheckPaisValid(putpais) then
			local put = {};
			put.paiArray = putpais;
			Network.Send(Protocal.ReqPutCard,protobuf.encode("ddz.PBPaiArray",put));
		else
			log("牌型不合法");
			this.Players[1]:ShowDownPais();
		end
	end);
	behaviour:AddClick(this.tipBtn,function()
		
	end);

	--初始化头像位置
	this.Players={};
	for i=1,3 do
		local path="Users/Player"..i;
		this.Players[i] = Player.new(transform:Find(path).gameObject);
	end

	--初始化座位号玩家
	for i,pb in ipairs(Game.users) do
		this.InitPlayer(pb);
	end
end

--seatId转index
function GamePanel.GetIndex(seatId)
	local div = seatId - Game.seatId;
	if(div < 0)then
		div = div + 3;
	end
	div = div + 1;
	return div;
end
--根据seatId找到玩家
function GamePanel.GetPlayer(seatId)
	local index = this.GetIndex(seatId);
	return this.Players[index];
end

function GamePanel.InitPlayer(pb)
	local player = this.GetPlayer(pb.seatId);
	player:SetName(pb.nickname);
	player:SetIcon(pb.icon);
	player:SetReady(pb.isReady);
end

function GamePanel.ClearPlayer(pb)
	local player = this.GetPlayer(pb.seatId);
	player:SetName("");
end

function GamePanel.AddReady(pb)
	local player = this.GetPlayer(pb.seatId);
	player:SetReady(true);

	if(pb.seatId==Game.seatId)then
		this.readyBtn:SetActive(false);
	end
end

function GamePanel.ClearReady(pb)
	local player = this.GetPlayer(pb.seatId);
	player:SetReady(false);
end
--设置地主
function GamePanel.Setlandlord(pb)
	for i=1,3 do
		this.Players[i]:SetIsRob(false);
		this.Players[i]:Setlandlord(false);
	end
	local index = this.GetPlayer(pb.seatId);
	this.Players[index]:Setlandlord(true);
end
--每局结束，清除游戏
function GamePanel.ClearGame()
	for i,player in ipairs(this.Players) do
		player:SetReady(false);
		player:Setlandlord(false);
		player:ClearOutView();
		player:ClearHandView();
	end
end
--初始化牌视图
function GamePanel.InitPaiArray(pb)
	this.exitBtn:SetActive(false);
	--只有自己有牌
	this.Players[1]:SetPaiArray(pb.paiArray);
end
--显示抢地主
function GamePanel.InitRobLandlord(robId)
	for i=1,3 do
		this.Players[i]:SetIsRob(false);
	end
	if Game.seatId == robId then
		this.ShowRobBtn(true);
	else
		local player = this.GetPlayer(robId);
		player:SetIsRob(true);
	end
end
--初始化地主状态
function GamePanel.InitLandlord(robId)
	for i=1,3 do
		this.Players[i]:SetIsRob(false);    --隐藏所有人抢地主状态
	end 
	local player = this.GetPlayer(robId);   --设置地主
	player:Setlandlord(true);
	this.ShowRobBtn(false);
end
--地主加三张牌
function GamePanel.InsertPai(pais)
	this.Players[1]:InsertPai(pais);
end
--抢地主按钮
function GamePanel.ShowRobBtn(isOn)
	this.robBtn:SetActive(isOn);
	this.norobBtn:SetActive(isOn);
end
--出牌按钮
function GamePanel.ShowPutBtn(isOn)
	this.passBtn:SetActive(isOn);
	this.putBtn:SetActive(isOn);
	this.tipBtn:SetActive(isOn);
end
--出牌阶段
function GamePanel.ShowPutBtns(seatId,canPass)
	log(seatId.."出牌阶段");
	for i=1,3 do
		this.Players[i]:SetPutStatus(false);
	end
	local player = this.GetPlayer(seatId);
	player:SetPutStatus(true);

	if Game.seatId == seatId then
		this.putBtn:SetActive(true);
		this.tipBtn:SetActive(true);

		this.selfCanPass = canPass;
		if canPass then
			this.passBtn:SetActive(true);
		end
	end
end
--不抢地主
function GamePanel.NoRobLandlord(seatId)
	log(seatId.."不抢地主");
	if Game.seatId == seatId then
		this.ShowRobBtn(false);
	end
end
--过牌
function GamePanel.PlayerPass(seatId)
	log(seatId.."过牌");
	if Game.seatId == seatId then
		this.Players[1]:ClearOutView();
		this.ShowPutBtn(false);
	end
end
--出牌
function GamePanel.PlayerPutCard(seatId,paiArray)
	log(seatId.."出牌");
	log(table.concat(paiArray,","));
	this.lastOutPais = paiArray;

	if Game.seatId == seatId then
		this.Players[1]:RemoveHandCard(paiArray);
		this.ShowPutBtn(false);
	end
	local player = this.GetPlayer(seatId);
	player:ClearOutView();      --清空出牌区  
	player:ShowOutCard(paiArray);    --出牌
end
--检测牌是否违法
function GamePanel.CheckPaisValid(pais)
	local paiType,count,dic = GetPaiType(pais);   --当前自己的牌
	if paiType == nil then
		return false;
	end
	if not this.selfCanPass then   --不能pass
		return true;
	else
		local paiType2,count2,dic2 = GetPaiType(this.lastOutPais);   --上家的牌
		if paiType ~= paiType2 then
			if paiType == PokerType.pkBB then
				return true;
			elseif paiType == PokerType.pk4 and paiType ~= PokerType.pkBB then
				return true;
			else
				return false;
			end
		end
		if count ~= count2 then
			return false;
		end
		return ComparePaiDic(dic,dic2);
	end
end