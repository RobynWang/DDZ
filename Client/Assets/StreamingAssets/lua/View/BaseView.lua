
BaseView=class("BaseView");

function BaseView:ctor(go)
   
end

-----------------------设置牌视图----------------------------
function BaseView:SetPaiArray(array)
    self.paiArray = array;
    log("BaseView:SetPaiArray");
    for i,paiId in ipairs(self.paiArray) do
		self:CreateCard(paiId);
    end
    self:RefreshPaiView();
end

function BaseView:CreateCard(paiId)
    local go = newObject(self.cardprefab);
    go.transform:SetParent(self.transform,false);
    go.transform.localPosition=Vector3(0,0,0);

    --初始化牌
    local pai = Card.new(go);
    pai:SetPai(paiId);
    self.cardList[paiId] = pai;
end

--刷新牌位置
function BaseView:RefreshPaiView()
    table.sort(self.paiArray,ComparePaiId);
    local total = #self.paiArray;

    for i,paiId in ipairs(self.paiArray) do
        local card=self.cardList[paiId];
        local x= (i - total/2 - 0.5) * 60;
        card.transform.localPosition=Vector3(x,0,0);
        card.transform:SetAsLastSibling();
        card.original=Vector3(x,0,0);
	end
end

function BaseView:RemovePais(pais)
    for i,paiId in ipairs(pais) do
        table.RemoveValue(self.paiArray,paiId);
        destroy(self.cardList[paiId].gameObject);
        self.cardList[paiId]=nil;
    end

    self:RefreshPaiView();
end

function BaseView:ClearPais()
    for i,paiId in ipairs(self.paiArray) do
        destroy(self.cardList[paiId].gameObject);
        self.cardList[paiId]=nil;
    end
    self.paiArray={};
end