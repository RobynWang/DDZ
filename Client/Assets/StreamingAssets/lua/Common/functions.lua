
--输出日志--
function log(str)
	Util.Log(str);
end

--错误日志--
function logError(str)
	Util.LogError(str);
end

--警告日志--
function logWarn(str)
	Util.LogWarning(str);
end

--查找对象--
function find(str)
	return GameObject.Find(str);
end

function destroy(obj)
	GameObject.Destroy(obj);
end

function newObject(prefab)
	return GameObject.Instantiate(prefab);
end

--创建面板--
function createPanel(name)
	PanelManager:CreatePanel(name);
end

function child(str)
	return transform:FindChild(str);
end

function subGet(childNode, typeName)		
	return child(childNode):GetComponent(typeName);
end

function findPanel(str)
	local obj = find(str);
	if obj == nil then
		error(str .. " is null");
		return nil;
	end
	return obj:GetComponent("BaseLua");
end

-- 获取牌名字
-- "c1"->"card_club_1"
function GetPaiName(paiId)
	local color = string.sub(paiId, 1, 1)
	local index = string.sub(paiId, 2)
	
	local path = ""
	if(color == "c") then--梅
		path = "card_club_";
	elseif(color == "d") then--方
		path = "card_diamond_";
	elseif(color == "h") then--红
		path = "card_heart_";
	elseif(color == "s") then--黑
		path = "card_spade_";
	elseif(color == "j") then--黑
		path = "card_joker_";
	end
	return path .. index;
end

--根据seatId计算 和 主角的相对位置
function RalativePos(seatId)
	local div = seatId - Global.localUser.seatId;
	if(div < 0) then
		div = div + 3;
	end
	return div + 1;
end

function table.removeValue(tb, key)
	local index = 0;
	for i, id in ipairs(tb) do
		if(key == id) then
			index = i;
			break;
		end
	end
	if(index > 0) then
		table.remove(tb, index);
	end
	return tb;
end

--------------------------------------------------------
--拆分字符
function string.split(s, delim)
	if type(delim) ~= "string" or string.len(delim) <= 0 then
	  return
	end
	local start = 1
	local t = {}
	while true do
		local pos = string.find (s, delim, start, true)
	  	if not pos then
	   		break
		end
		table.insert (t, string.sub (s, start, pos - 1))
		start = pos + string.len (delim)
	end
	table.insert (t, string.sub (s, start))
	return t
end

function Vec2Str(uvector)
	return uvector.x,uvector.y;
end

function table.RemoveValue(tb,value)
	for k,v in pairs(tb) do
		if v == value then
			table.remove(tb,k);
			return;
		end
	end
end
--------------------------------------------------------

--比较牌大小
function ComparePaiId(a, b)
	local color1,numA = SplitPai(a);
	local color2,numB = SplitPai(b);
	if numA == numB then
		return color1 > color2;
	else
		return numA > numB;
	end
end 
--拆分花色和数字
function SplitPai(paiId)
	local array = string.split(paiId,"_");
	local num = tonumber(array[2]);
	num = num < 3 and num + 13 or num;
	return tonumber(array[1]),num;
end
--获取牌num
function PaiNum(paiId)
	local array = string.split(paiId,"_");
	local num = tonumber(array[2]);
	num = num < 3 and num + 13 or num;
	return num;
end
--牌大小全部一样
function AllSame(pais)
	local color1,num1 = SplitPai(pais[1]);
	for i=2,#pais do
		local color2,num2 = SplitPai(pais[i]);
		if num1 ~= num2 then
			return false;
		end
	end
	return true;
end
--获取相同牌的数量
function PaiDic(pais)
    local last = PaiNum(pais[1]);
    local count=1;
    local dic={{},{},{},{}};
    for i=2,#pais do
        local num = PaiNum(pais[i]);
        if(last ~= num)then
            table.insert(dic[count], last);
            count=1;
            last=num;
        else
            count=count+1;
        end
    end
    table.insert(dic[count], last);
    return dic;
end
--是否是炸弹
function IsBomb(pais)
    if(#pais==2)then
        local color1,num1=SplitPai(pais[1]);
		local color2,num2=SplitPai(pais[2]);
		if(num1+num2==35)then
			return true;
        end
    elseif(#pais==4)then
        return AllSame(pais);
    end
    return false;
end
--枚举牌类型
PokerType={
	pk1=1,          --单张
    pk2=2,          --一对
    pk3=3,          --三张不带
    pk3_1=4,        --三带一
    pk3_2=5,        --三带二
    pk4=6,          --炸弹
    pk4_11=7,       --四带两单
    pk4_22=8,       --四带一对
    pk12345=9,      --顺子
    pk112233=10,    --连对
    pk33=11,        --飞机
    pk33_11=12,     --飞机带单
    pk33_22=13,     --飞机带对
    pkBB=14         --王炸
}
--检测出牌类型是否合法
function GetPaiType(pais)

	local count = #pais;
	local dic = PaiDic(pais);
	local paiType = nil;

	if count == 1 then
		paiType = PokerType.pk1;
	elseif count == 2 then
		if AllSame(pais) then
			paiType = PokerType.pk2;
		elseif IsBomb(pais) then
			paiType = PokerType.pkBB;
		end
	elseif count == 3 then
		if AllSame(pais) then
			paiType = PokerType.pk3;
		end
	elseif count == 4 then
        if IsBomb(pais) then
			paiType = PokerType.pk4;
        elseif(#dic[3]==1)then
            paiType = PokerType.pk3_1;
        end
    elseif count == 5 then
        if(#dic[3]==1 and #dic[2]==1)then
            paiType = PokerType.pk3_2;
        end
    elseif count == 6 then
        if(#dic[4]==1 and #dic[1]==2 or #dic[2]==1)then
            paiType = PokerType.pk4_11;
        end
    elseif count == 8 then
        if(#dic[3]==2 and #dic[1]==2 and dic[3][1]==dic[3][2]+1)then
            paiType = PokerType.pk33_11;
        elseif(#dic[4]==1 and #dic[2]==2)then
            paiType = PokerType.pk4_22;
        end
    elseif count == 10 then
        if(#dic[3]==2 and #dic[2]==2 and dic[3][1]==dic[3][2]+1)then
            paiType = PokerType.pk33_22;
        end
    elseif count == 12 then
        if(#dic[3]==3 and #dic[1]==3 and dic[3][1]==dic[3][3]+2)then
            paiType = PokerType.pk33_11;
		end
	end
	--判断顺子，连对和飞机
	if paiType == nil and count >= 5 then
		if #dic[1] == count then          --顺子
			if dic[1][1] ~= 15 and dic[1][1] == dic[1][count]+count-1 then
				paiType = PokerType.pk12345;
			end
		elseif #dic[2] == count/2 then    --连对
			if dic[2][1] ~= 15 and dic[2][1] == dic[2][count/2] + count/2-1 then
				paiType = PokerType.pk112233;
			end
		elseif #dic[3] == count/3 then    --飞机
			if dic[3][1] ~= 15 and dic[3][1] == dic[3][count/3]+count/3-1 then
				paiType = PokerType.pk33;
			end
		end
	end
	return paiType,count,dic;
end
--比较牌大小
function ComparePaiDic(dic1,dic2)
	if(#dic1[4]>0)then
        return dic1[4][1]>dic2[4][1];
    elseif(#dic1[3]>0)then
        return dic1[3][1]>dic2[3][1];
    elseif(#dic1[2]>0)then
        return dic1[2][1]>dic2[2][1];
    else
        return dic1[1][1]>dic2[1][1];
    end
end
