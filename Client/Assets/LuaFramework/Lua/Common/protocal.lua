--Buildin Table
Protocal = {
	Connect		= '101';	--连接服务器
	Exception   = '102';	--异常掉线
	Disconnect  = '103';	--正常断线   
	Message		= '104';	--接收消息


	ReqLogin	 		= 1001;	--登录
	ReqEnterRoom 		= 1002;	--进入房间
	ReqOtherEnterRoom 	= 1003;	--其他玩家进入房间
	ReqExitRoom 		= 1004;	--退出房间
	ReqOtherExitRoom 	= 1005;	--其他玩家退出房间
	ReqChat		 		= 1006;	--聊天

	ReqReady			= 2001; --准备游戏
	ReqGameStart		= 2002; --开始游戏
	ReqRobLandlord		= 2003;	--抢地主
	ReqNoRobLandlord    = 2004;	--不抢地主
	ReqGiveCard			= 2005;	--发牌
	ReqPutCard			= 2006; --出牌
	ReqPass				= 2007; --不要
	ReqGameOver			= 2008; --游戏结算
	ReqShowRobLandlord  = 2009, --显示抢地主按钮
    ReqShowPutCard      = 2010, --显示该谁出牌
    ReqContinue         = 2011, --继续下一句游戏

	ReqError			= 5001;	--异常
}



ErrorId={}
ErrorId[101]="账户重复登录";
ErrorId[102]="账户不存在";
ErrorId[103]="密码错误";