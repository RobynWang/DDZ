
CtrlNames = {
	Prompt = "PromptCtrl",
	Message = "MessageCtrl"
}

PanelNames = {
	"PromptPanel",	
	"MessagePanel",
}

--协议类型--
ProtocalType = {
	BINARY = 0,
	PB_LUA = 1,
	PBC = 2,
	SPROTO = 3,
}
--当前使用的协议类型--
TestProtoType = ProtocalType.BINARY;

Util = LuaFramework.Util;
AppConst = LuaFramework.AppConst;
LuaHelper = LuaFramework.LuaHelper;
ByteBuffer = LuaFramework.ByteBuffer;

resMgr = LuaHelper.GetResManager();
panelMgr = LuaHelper.GetPanelManager();
soundMgr = LuaHelper.GetSoundManager();
networkMgr = LuaHelper.GetNetManager();
ssdk = LuaHelper.GetSSDKManager();

WWW = UnityEngine.WWW;
GameObject = UnityEngine.GameObject;
Sprite = UnityEngine.Sprite;
Input = UnityEngine.Input;
RectTransformUtility = UnityEngine.RectTransformUtility;
PlatformType = cn.sharesdk.unity3d.PlatformType;
ResponseState = cn.sharesdk.unity3d.ResponseState;
ShareContent = cn.sharesdk.unity3d.ShareContent;
Rect = UnityEngine.Rect;