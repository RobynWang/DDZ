GameHandler={};
local this=GameHandler;

function GameHandler.Init(net)
    net.Handlers[Protocal.ReqReady] = this.OnReady;
    net.Handlers[Protocal.ReqGameStart] = this.OnGameStart;
    net.Handlers[Protocal.ReqRobLandlord] = this.OnRobLandlord;
    net.Handlers[Protocal.ReqNoRobLandlord] = this.OnNoRobLandlord;
    net.Handlers[Protocal.ReqPutCard] = this.OnPutCard;
    net.Handlers[Protocal.ReqPass] = this.OnPass;
    net.Handlers[Protocal.ReqGiveCard] = this.OnGiveCard;
    net.Handlers[Protocal.ReqShowRobLandlord] = this.OnShowRobLandlord;
    net.Handlers[Protocal.ReqShowPutCard] = this.OnShowPutCard;
    net.Handlers[Protocal.ReqGameOver] = this.OnGameOver;
    net.Handlers[Protocal.ReqContinue]=this.OnGameContinue;
end

function GameHandler.OnReady(data)
    local pb = protobuf.decode("ddz.PBUser",data);
    GamePanel.AddReady(pb);
end

function GameHandler.OnGameStart(data)
    local pb = protobuf.decode("ddz.PBPaiArray",data);
    GamePanel.InitPaiArray(pb);
    GamePanel.InitRobLandlord(pb.seatId);
end

function GameHandler.OnRobLandlord(data)
    local pb = protobuf.decode("ddz.PBSeatId",data);
    GamePanel.InitLandlord(pb.seatId);
end

function GameHandler.OnNoRobLandlord(data)
    local pb = protobuf.decode("ddz.PBSeatId",data);
    GamePanel.NoRobLandlord(pb.seatId);
end
function GameHandler.OnPass(data)
    local pb = protobuf.decode("ddz.PBSeatId",data);
    GamePanel.PlayerPass(pb.seatId);
end

function GameHandler.OnPutCard(data)
    local pb = protobuf.decode("ddz.PBPaiArray",data);
    GamePanel.PlayerPutCard(pb.seatId,pb.paiArray);
end

function GameHandler.OnGiveCard(data)
    local pb = protobuf.decode("ddz.PBPaiArray",data);
    GamePanel.InsertPai(pb.paiArray);
    GamePanel.ShowPutBtns(Game.seatId, false);
end
function GameHandler.OnShowRobLandlord(data)
    local pb = protobuf.decode("ddz.PBSeatId",data);
    GamePanel.InitRobLandlord(pb.seatId);
end
function GameHandler.OnShowPutCard(data)
    local pb=protobuf.decode("ddz.PBPut",data);
    GamePanel.ShowPutBtns(pb.seatId,pb.canPass);
end

function GameHandler.OnGameOver(data)
    local pb=protobuf.decode("ddz.PBEnd",data);
    log("游戏结束");
    ResultPanel.New(pb.results);
end

function GameHandler.OnGameContinue(data)
    ResultPanel.Close();
    GamePanel.ClearGame();
end
