

Player=class("Player");

function Player:ctor(go)
    self.gameObject=go;
    self.transform=go.transform;

    self.icon = go:GetComponent('Image');
    self.nameText = go.transform:Find("nameText"):GetComponent('Text');
    self.scoreText = go.transform:Find("scoreText"):GetComponent('Text');
    self.status = go.transform:Find("Ready"):GetComponent("Text");
    self.landlord = go.transform:Find("landlord").gameObject;

    self.paiView = PlayerHandView.new(go.transform:Find("paiView").gameObject);
    self.outView = PlayerOutView.new(go.transform:Find("outView").gameObject);

end

function Player:SetName(name)
    self.nameText.text=name;
end
function Player:SetIcon(url)
   
    if url == "" then
        return;
    end
    print(url);
    coroutine.start(self.DownLoad,self,url);
end

function Player:DownLoad(url)
    local www = WWW(url);
    coroutine.www(www);
    self.icon.sprite = Sprite.Create(www.texture, Rect(0,0,www.texture.width,www.texture.height), Vector2(0.5,0.5));
end


function Player:SetScore(score)
    self.scoreText.text=score;
end
function Player:SetReady(isOn)
    if isOn then
        self.status.text = "准备";
    else 
        self.status.text = "";
    end
end
function Player:Setlandlord(isOn)
    self.landlord:SetActive(isOn);
end
function Player:SetIsRob()
    if isOn then
        self.status.text = "抢地主";
    else 
        self.status.text = "";
    end
end
function Player:SetPutStatus(isOn)
    if isOn then
        self.status.text = "出牌中";
    else
        self.status.text = "";
    end
end

-------handView------

function Player:SetPaiArray(array)
    self.paiView:SetPaiArray(array);
end
--插入三张地主牌
function Player:InsertPai(array)
    self.paiView:InsertPaiArray(array);
end
--得到点击后的牌组
function Player:GetPressPai()
    return self.paiView:GetShootPais();
end
function Player:RemoveHandCard(array)
    self.paiView:RemovePais(array);
end
function Player:ShowDownPais()
    self.paiView:ShowDownPais();
end
function Player:ClearHandView()
    self.paiView:ClearPais()
end
-------------------------OutView----------------
function Player:ShowOutCard(array)
    self.outView:SetPaiArray(array);
end
function Player:ClearOutView()
    self.outView:ClearPais()
end