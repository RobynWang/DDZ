LoginPanel = {};
local this = LoginPanel;

local gameObject;
local transform;
local behaviour;

function LoginPanel.New()
	panelMgr:CreatePanel("Login", nil);
end

function LoginPanel.Close()
	panelMgr:ClosePanel("Login");
end

function LoginPanel.Awake(obj)
	gameObject = obj;
	transform = obj.transform;
	behaviour = gameObject:GetComponent('LuaBehaviour');
	
	this.username = transform:Find("username"):GetComponent("InputField");
	this.loginBtn = transform:Find("LoginBtn").gameObject;

	this.qqBtn = transform:Find("qqBtn").gameObject;
	this.wxBtn = transform:Find("wxBtn").gameObject;
	this.shareBtn = transform:Find("shareBtn").gameObject;
	
	if Util.IsMobile then
		this.username.gameObject:SetActive(false);
		this.loginBtn:SetActive(false);
	else
		this.qqBtn:SetActive(false);
		this.wxBtn:SetActive(false);
	end

	behaviour:AddClick(this.loginBtn, this.OnLoginClick);

	behaviour:AddClick(this.qqBtn, this.OnQQClick);
	behaviour:AddClick(this.wxBtn, this.OnWXClick);
	behaviour:AddClick(this.shareBtn, this.OnShareClick);

	ssdk.authHandler = this.OnAuthResultHandler;
	ssdk.shareHandler = this.OnShareResultHandler;
end

function LoginPanel.OnLoginClick()
	local acc={};
	acc.userId = this.username.text;
	acc.nickname = this.nickname.text;
	
    local data = protobuf.encode("ddz.PBAccount",acc);
    Network.Send(Protocal.ReqLogin, data)
end

function LoginPanel.OnQQClick()
	ssdk:Authorize(PlatformType.QQ);
end
function LoginPanel.OnWXClick()
	ssdk:Authorize(PlatformType.WeChat);
end
function LoginPanel.OnShareClick()
	print(ShareContent);
	ShareContent.SetText("斗地主");
	ShareContent.SetImageUrl("http://ww3.sinaimg.cn/mw690/be159dedgw1evgxdt9h3fj218g0xctod.jpg");
	ShareContent.SetUrl("http://www.mob.com");
	ssdk:ShareContent(PlatformType.QQ, ShareContent);
end

--授权成功回调
function LoginPanel.OnAuthResultHandler(reqID, state, type, result)
	if (state == ResponseState.Success) then
		if (result ~= nil and result.Count > 0) then
			print ("authorize success !");
			local json = MiniJSON.jsonEncode(result);
			local user = cjson.decode(json);

			local acc = {};
			acc.userId = user.userID;
			acc.nickname = user.nickname;
			acc.icon = user.icon;
			local data = protobuf.encode("ddz.PBAccount",acc);
			Network.Send(Protocal.ReqLogin, data)
		end
	elseif (state == ResponseState.Fail) then
		print ("fail! throwable stack = ");
	elseif (state == ResponseState.Cancel) then
		print ("cancel !");
	end
end

function LoginPanel.OnShareResultHandler(reqID, state, type, result)
	if (state == ResponseState.Success) then
		print ("share successfully - share result :");
		print (MiniJSON.jsonEncode(result));
	elseif (state == ResponseState.Fail) then
		print ("fail! throwable stack = ");
	elseif (state == ResponseState.Cancel) then
		print ("cancel !");
	end
end