require("class");
require("Common/define");
require("Common/functions");
require "Common/protocal"
cjson = require "cjson"
util = require "3rd/cjson/util"
Event = require 'events'

require "3rd/pblua/login_pb"
require "3rd/pbc/protobuf"

require("Network/LoginHandler");
require("Network/HallHandler");
require("Network/GameHandler");

require("View/Card")
require("View/Player");
require("View/LoginPanel");
require("View/HallPanel");
require("View/GamePanel");
require("View/ResultPanel");

require("View/BaseView");
require("View/PlayerHandView");
require("View/PlayerOutView");

--主入口函数。从这里开始lua逻辑
function Main()					
	local breakSocketHandle, debugXpCall = require("LuaDebugjit")("localhost", 7003)
	local timer = Timer.New(function()
		breakSocketHandle() end, 1, - 1, false)
	timer:Start();
end

--场景切换通知
function OnLevelWasLoaded(level)
	collectgarbage("collect")
	Time.timeSinceLevelLoad = 0
end 