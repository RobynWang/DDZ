function class(classname, ...)     -- 参数一：要创建的类名称，参数二：父类-可选参数，可以是table，function，userdata等
    local cls = {__cname = classname}

    local supers = {...}   
    for _, super in ipairs(supers) do    -- 遍历父类
        local superType = type(super)
        -- 父类如果不是第一个参数表示的类型的话，就输出第二个参数
        assert(superType == "nil" or superType == "table" or superType == "function",
            string.format("class() - create class \"%s\" with invalid super class type \"%s\"",
                classname, superType))

        if superType == "function" then
            assert(cls.__create == nil,
                string.format("class() - create class \"%s\" with more than one creating function",
                    classname));
            -- 如果父类是个function的话，就让cls的create方法指向他
            cls.__create = super
        elseif superType == "table" then  -- 如果父类是table
            if super[".isclass"] then
                -- 如果父类是原生的cocos类，比如cc.Node，不是自己定义的类
                assert(cls.__create == nil,
                    string.format("class() - create class \"%s\" with more than one creating function or native class",
                        classname));
                cls.__create = function() return super:create() end
            else
                -- 如果父类是自定义的类，比如 A = {}
                cls.__supers = cls.__supers or {}
                cls.__supers[#cls.__supers + 1] = super       -- 保存cls的多个父类的table表
                if not cls.super then
                    -- 将遍历到的第一个table作为cls的父类
                    cls.super = super
                end
            end
        else
            -- 如果父类既不是table，也不是function的话，报错。如果父类不存在，不会执行这个循环的，所以super可以为nil。
            error(string.format("class() - create class \"%s\" with invalid super type",
                        classname), 0)
        end
    end
    -- 前面一大段是找到cls的所有父类；
    -- 接下来就是设置cls的元表了。
    -- 设置cls的第一索引对象是自己，如果实例对象找不到某个参数的话，
    --就会查找该类是否包含该参数，如果该类也不包含的话，就去父类查找
    cls.__index = cls   
    if not cls.__supers or #cls.__supers == 1 then  
        -- 如果cls只有一个父类，即单继承的话，设置cls的父类是他的元表
        setmetatable(cls, {__index = cls.super})
    else
        -- 如果cls是多重继承的话，__index为一个函数，找元素的时候就会执行一遍该函数
        setmetatable(cls, {__index = function(_, key)
            -- 遍历所有父类，查找key对应的值并返回
            local supers = cls.__supers
            for i = 1, #supers do
                local super = supers[i]
                if super[key] then return super[key] end
            end
        end})
    end

    if not cls.ctor then
        -- 增加一个默认的构造函数
        cls.ctor = function() end
    end
    cls.new = function(...)  -- 新建方法，这个也是比较重要的方法
        local instance
        if cls.__create then
            -- 如果有create方法，那么就调用，正常情况下，自定义的cls是没有create方法的。
            -- 通过__index和元表的index，一级一级往上找，直到找到原生cocos类
            instance = cls.__create(...)
        else
            instance = {}  -- 没有create方法，他是一个普通类
        end
        -- 设置instance的元表index，谁调用了new，就将他设置为instance的元类index
        setmetatableindex(instance, cls)
        instance.class = cls
        instance:ctor(...) --调用构造方法
        return instance
    end
    cls.create = function(_, ...)
        return cls.new(...)
    end

    return cls
end

setmetatableindex = function(t, index)
    if type(t) == "userdata" then
        local peer = tolua.getpeer(t)
        if not peer then
            peer = {}
            tolua.setpeer(t, peer)
        end
        setmetatableindex(peer, index)
    else
        local mt = getmetatable(t)
        if not mt then mt = {} end
        if not mt.__index then
            mt.__index = index
            setmetatable(t, mt)
        elseif mt.__index ~= index then
            setmetatableindex(mt, index)
        end
    end
end