﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using LuaInterface;

public class LuaPoint : MonoBehaviour ,IPointerDownHandler,IPointerUpHandler,IDragHandler{

    LuaTable table;
    LuaFunction downHandler;
    LuaFunction upHandler;
    LuaFunction dragHandler;

    #region 静态lua表使用
    public void AddDownHandler(LuaFunction func)
    {
        downHandler = func;
    }
    public void AddUpHandler(LuaFunction func)
    {
        upHandler = func;
    }
    public void AddDragHandler(LuaFunction func)
    {
        dragHandler = func;
    }
    #endregion

    #region 面向对象class使用
    public void AddDownHandler(LuaFunction func, LuaTable tb)
    {
        downHandler = func;
        table = tb;
    }
    public void AddUpHandler(LuaFunction func, LuaTable tb)
    {
        upHandler = func;
        table = tb;
    }
    public void AddDragHandler(LuaFunction func, LuaTable tb)
    {
        dragHandler = func;
        table = tb;
    }
    #endregion

    public void OnDrag(PointerEventData eventData)
    {
        if (dragHandler != null)
        {
            if (table != null)
                dragHandler.Call(table);
            else
                dragHandler.Call();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (upHandler != null)
        {
            if (table != null)
                upHandler.Call(table);
            else
                upHandler.Call();
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (downHandler != null)
        {
            if (table != null)
                downHandler.Call(table);
            else
                downHandler.Call();
        }
    }
}
